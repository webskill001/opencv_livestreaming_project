FROM python:3
ENV PYTHONNUMBUFFERED 1
RUN mkdir /app
WORKDIR /app
COPY requirement.txt /app/requirement.txt
RUN pip install -r requirement.txt
COPY . /app